#!/usr/bin/env bash

OWNER=phacility
REFSPEC=stable

PHABRICATOR_COMMIT=$(curl -s "https://api.github.com/repos/${OWNER}/phabricator/commits/${REFSPEC}" --header "Accept: application/vnd.github.v3.sha")
ARCANIST_COMMIT=$(curl -s "https://api.github.com/repos/${OWNER}/arcanist/commits/${REFSPEC}" --header "Accept: application/vnd.github.v3.sha")
LIBPHUTIL_COMMIT=$(curl -s "https://api.github.com/repos/${OWNER}/libphutil/commits/${REFSPEC}" --header "Accept: application/vnd.github.v3.sha")

echo "PHABRICATOR_COMMIT=${PHABRICATOR_COMMIT}"
echo "ARCANIST_COMMIT=${ARCANIST_COMMIT}"
echo "LIBPHUTIL_COMMIT=${LIBPHUTIL_COMMIT}"

docker build --pull \
  --build-arg PHABRICATOR_COMMIT=${PHABRICATOR_COMMIT} \
  --build-arg ARCANIST_COMMIT=${ARCANIST_COMMIT} \
  --build-arg LIBPHUTIL_COMMIT=${LIBPHUTIL_COMMIT} \
  -t "imageleaf/phabricator:dev" .
